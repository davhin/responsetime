%example call: folder_response('./data/responder/');

function folder_response(directory)

filename = directory;
set_parameters;

%filename=input('Enter Directory ','s');
op=strcat(filename,'\*.wav');
directory=dir(op);


resultfile=strcat('\ResponseTime.XLS');
finalresultfile=strcat(filename,resultfile);
filehandle=fopen(finalresultfile,'w');


loop = 1;
process = 1;
while (loop==1)
    file=strcat(filename,'/',directory(process).name);
    [resp, ons] = response(file, process, directory(process).name); 
    fprintf(filehandle,'%s\t  %s\t %s\t \n',directory(process).name,num2str(resp),num2str(ons));

    if(process==length(directory))
        break;
    else
    process=process+1;
    end

end

fclose(filehandle);



end